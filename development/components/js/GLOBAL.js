//============================
//    Name: index.js
//============================

// import for others scripts to use
window.$ = $;
window.jQuery = jQuery;

// import 'babel-polyfill';
// import 'owl.carousel';
// import 'jquery-scrollify/jquery.scrollify';

$(() => {

  /* ======= Global Actions ======= */
  const
    k = 'click',
    act = 'item-true',
    dis = 'item-false';
  /* ======= ---- ======= */



  /* ======= list ======= */
  const ___order__list_ = () => {
    pageMain.__load();
  };
  /* ======= ---- ======= */

  const pageMain = {
    __load() {
      // let regexpEmail = /\S+[@]\S+[.]\w+$/gsi;

      let setting = {

        // * Run functions once
        __firstRuns() {
          this.example();
        },
        // * Handlers
        __handlers() {
          $(window).on('scroll', () => this.scrollUPShow());
          $('.js__scrollUP').on(k, () => $(window).scrollTop(0));
        },

        /* ======= Code ======= */
        example() {
          console.log(`HelloWorld ${act}`);
          console.log(`HelloWorld ${dis}`);
        },

        scrollUPShow() {
          $(window).scrollTop() >= 100
            ? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act)
            : $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
        },

        /* ======= ---- ======= */




        // * Loaders 
        __loaders() {
          this.__firstRuns();
          this.__handlers();
        }
      };
      setting.__loaders();
    }
  };
  /* ======= list ======= */
  ___order__list_();
  /* ======= ---- ======= */
});