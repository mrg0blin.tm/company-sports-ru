/* ==== IMPORT PARAMS ==== */
'use strict';
import { lastRun } from 'gulp';
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inDev = 'development',
inPub = 'public';
/* ==== ----- ==== */


/* ==== Replace URL or Links ==== */
const __link = {
	srcpath: {
		in:  'src="./components/img/',
		out:  'src="../media/img/'
	}
};
/* ==== ----- ==== */

/* ==== Replace URL or Links ==== */
const __cfg = {
	include: {
		prefix: '<!-- @@',
		suffix: '-->',
		basepath: '@file'
	}
};
/* ==== ----- ==== */

let sinceReplace = (x) => `${x}`.replace(/-/gi, ':');

module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	() => combiner(
	
		src(`${inDev}/*.html`, { since: lastRun(sinceReplace(nameTask))}),
		_run.cached(nameTask),
		_run.htmlReplace(__link.srcpath.in, __link.srcpath.out),
		_run.include(__cfg.include),
		_run.remember(nameTask),
		_run.newer(inPub),
		dest(inPub)
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));

  