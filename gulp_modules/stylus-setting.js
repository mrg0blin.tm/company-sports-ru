/* ==== IMPORT PARAMS ==== */
'use strict';
import resolver from 'stylus';
import px2rem from 'stylus-px2rem';
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
	inDev = 'development',
	inDevApps = `${inDev}/components`,
	inPub = 'public',
	inPubCss = `${inPub}/css`;
/* ==== ----- ==== */


/* ==== Replace URL or Links ==== */
const __cfg = {
	autoprefixer: {
		dev: {
			grid: false,
			cascade: false,
			flexbox: false,
			remove: true,
			browsers: ['last 5 version']
		},
		pub: {
			grid: true,
			cascade: true,
			flexbox: true,
			remove: true,
			browsers: ['last 5 version']
		}
	},
	stylus: {
		sprite: {
			import: `${__dirname}/../${inDev}/tmp/sprite`,
			define: { url: resolver() }
		},
		pixelsize: { use: px2rem() }
	},
};
/* ==== ----- ==== */


/* ==== Replace URL or Links ==== */
const __link = {
	css: {
		image: {
			prepend: '../media/img/'
		},
		fonts: {
			replace: ['../media/img/fonts/', '../media/fonts/']
		}
	}
};
/* ==== ----- ==== */


module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src(`${inDevApps}/stylus/**/connect.styl`),
		_run.if(isDevelopment, _run.sourcemaps.init()),
		_run.stylus(__cfg.stylus.sprite, __cfg.stylus.pixelsize),
		_run.if(isDevelopment, _run.autoprefixer(__cfg.autoprefixer.dev)),
		_run.if(isPublic, _run.autoprefixer(__cfg.autoprefixer.pub)),
		_run.if(isDevelopment, _run.sourcemaps.write()),
		_run.urlReplace(__link.css.image),
		_run.urlReplace(__link.css.fonts),
		_run.rename('global.css'),
		dest(inPubCss))
	.on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));