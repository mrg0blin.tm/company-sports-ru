webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, jQuery, __webpack_provided_window_dot_jQuery) {

//============================
//    Name: index.js
//============================
// import for others scripts to use
window.$ = $;
__webpack_provided_window_dot_jQuery = jQuery; // import 'babel-polyfill';
// import 'owl.carousel';
// import 'jquery-scrollify/jquery.scrollify';

$(function () {
  /* ======= Global Actions ======= */
  var k = 'click',
      act = 'item-true',
      dis = 'item-false';
  /* ======= ---- ======= */

  /* ======= list ======= */

  var ___order__list_ = function ___order__list_() {
    pageMain.__load();
  };
  /* ======= ---- ======= */


  var pageMain = {
    __load: function __load() {
      // let regexpEmail = /\S+[@]\S+[.]\w+$/gsi;
      var setting = {
        // * Run functions once
        __firstRuns: function __firstRuns() {
          this.example();
        },
        // * Handlers
        __handlers: function __handlers() {
          var _this = this;

          $(window).on('scroll', function () {
            return _this.scrollUPShow();
          });
          $('.js__scrollUP').on(k, function () {
            return $(window).scrollTop(0);
          });
        },

        /* ======= Code ======= */
        example: function example() {
          console.log("HelloWorld ".concat(act));
          console.log("HelloWorld ".concat(dis));
        },
        scrollUPShow: function scrollUPShow() {
          $(window).scrollTop() >= 100 ? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act) : $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
        },

        /* ======= ---- ======= */
        // * Loaders 
        __loaders: function __loaders() {
          this.__firstRuns();

          this.__handlers();
        }
      };

      setting.__loaders();
    }
  };
  /* ======= list ======= */

  ___order__list_();
  /* ======= ---- ======= */

});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0), __webpack_require__(0), __webpack_require__(0)))

/***/ })
],[1]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2xvYmFsLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvZ2xvYmFsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4vLyAgICBOYW1lOiBpbmRleC5qc1xyXG4vLz09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuXHJcbi8vIGltcG9ydCBmb3Igb3RoZXJzIHNjcmlwdHMgdG8gdXNlXHJcbndpbmRvdy4kID0gJDtcclxud2luZG93LmpRdWVyeSA9IGpRdWVyeTtcclxuXHJcbi8vIGltcG9ydCAnYmFiZWwtcG9seWZpbGwnO1xyXG4vLyBpbXBvcnQgJ293bC5jYXJvdXNlbCc7XHJcbi8vIGltcG9ydCAnanF1ZXJ5LXNjcm9sbGlmeS9qcXVlcnkuc2Nyb2xsaWZ5JztcclxuXHJcbiQoKCkgPT4ge1xyXG5cclxuXHQvKiA9PT09PT09IEdsb2JhbCBBY3Rpb25zID09PT09PT0gKi9cclxuXHRjb25zdFxyXG5cdFx0ayA9ICdjbGljaycsXHJcblx0XHRhY3QgPSAnaXRlbS10cnVlJyxcclxuXHRcdGRpcyA9ICdpdGVtLWZhbHNlJztcclxuXHQvKiA9PT09PT09IC0tLS0gPT09PT09PSAqL1xyXG5cclxuIFxyXG5cclxuXHQvKiA9PT09PT09IGxpc3QgPT09PT09PSAqL1xyXG5cdGNvbnN0IF9fX29yZGVyX19saXN0XyA9ICgpID0+IHtcclxuXHRcdHBhZ2VNYWluLl9fbG9hZCgpO1xyXG5cdH07XHJcblx0LyogPT09PT09PSAtLS0tID09PT09PT0gKi9cclxuXHJcblx0Y29uc3QgcGFnZU1haW4gPSB7XHJcblx0XHRfX2xvYWQoKSB7XHJcblx0XHRcdC8vIGxldCByZWdleHBFbWFpbCA9IC9cXFMrW0BdXFxTK1suXVxcdyskL2dzaTtcclxuXHJcblx0XHRcdGxldCBzZXR0aW5nID0ge1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vICogUnVuIGZ1bmN0aW9ucyBvbmNlXHJcblx0XHRcdFx0X19maXJzdFJ1bnMoKSB7XHJcblx0XHRcdFx0XHR0aGlzLmV4YW1wbGUoKTtcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdC8vICogSGFuZGxlcnNcclxuXHRcdFx0XHRfX2hhbmRsZXJzKCkge1xyXG5cdFx0XHRcdFx0JCh3aW5kb3cpLm9uKCdzY3JvbGwnLCAoKSA9PiB0aGlzLnNjcm9sbFVQU2hvdygpKTtcclxuXHRcdFx0XHRcdCQoJy5qc19fc2Nyb2xsVVAnKS5vbihrLCAoKSA9PiAkKHdpbmRvdykuc2Nyb2xsVG9wKDApKTtcclxuXHRcdFx0XHR9LFxyXG5cclxuXHRcdFx0XHQvKiA9PT09PT09IENvZGUgPT09PT09PSAqL1xyXG5cdFx0XHRcdGV4YW1wbGUoKSB7XHJcblx0XHRcdFx0XHRjb25zb2xlLmxvZyhgSGVsbG9Xb3JsZCAke2FjdH1gKTtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGBIZWxsb1dvcmxkICR7ZGlzfWApO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0XHJcblx0XHRcdFx0c2Nyb2xsVVBTaG93KCkge1xyXG5cdFx0XHRcdFx0JCh3aW5kb3cpLnNjcm9sbFRvcCgpID49IDEwMFxyXG5cdFx0XHRcdFx0XHQ/ICQoJy5qc19fc2Nyb2xsVVAnKS5yZW1vdmVDbGFzcyhhY3QgKyAnLW91dCcpICsgJCgnLmpzX19zY3JvbGxVUCcpLmFkZENsYXNzKGFjdClcclxuXHRcdFx0XHRcdFx0OiAkKCcuanNfX3Njcm9sbFVQJykuYWRkQ2xhc3MoYWN0ICsgJy1vdXQnKSArICQoJy5qc19fc2Nyb2xsVVAnKS5yZW1vdmVDbGFzcyhhY3QpO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0XHJcblx0XHRcdFx0LyogPT09PT09PSAtLS0tID09PT09PT0gKi9cclxuXHJcblxyXG5cclxuXHJcblx0XHRcdFx0Ly8gKiBMb2FkZXJzIFxyXG5cdFx0XHRcdF9fbG9hZGVycygpIHtcclxuXHRcdFx0XHRcdHRoaXMuX19maXJzdFJ1bnMoKTtcclxuXHRcdFx0XHRcdHRoaXMuX19oYW5kbGVycygpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcdFx0c2V0dGluZy5fX2xvYWRlcnMoKTtcclxuXHRcdH1cclxuXHR9O1xyXG5cdC8qID09PT09PT0gbGlzdCA9PT09PT09ICovXHJcblx0X19fb3JkZXJfX2xpc3RfKCk7XHJcblx0LyogPT09PT09PSAtLS0tID09PT09PT0gKi9cclxufSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIGRldmVsb3BtZW50L2NvbXBvbmVudHMvanMvZ2xvYmFsLmpzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQWpDQTtBQUNBO0FBa0NBO0FBQ0E7QUF4Q0E7QUEwQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=